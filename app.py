from flask import Flask, render_template, request
from flask_bootstrap import Bootstrap
import datetime
import re
import logging
import logging.config
import yaml
from glob import glob
from netaddr import IPNetwork
import redis
from pprint import pprint
import time

app = Flask(__name__)
Bootstrap(app)


def get_config():
    with open("config.yaml", "r") as config_stream:
        config = yaml.safe_load(config_stream)

    return config


config = get_config()

my_redis = redis.Redis(
    host=config["redis"]["host"],
    port=config["redis"]["port"],
    password=config["redis"]["pass"],
    db=0,
    decode_responses=True,
)


def all_same(items):
    it = iter(items)
    first = next(it, None)
    return all(x == first for x in it)


def network(address):
    ip = IPNetwork(address)
    prefix = ip.network
    prefixlen = ip.prefixlen
    return "{}/{}".format(str(prefix), str(prefixlen))


def get_link_interfaces(VARS_PATH):
    interfaces = {}
    for filename in glob(VARS_PATH):
        with open(filename) as f:
            data = yaml.safe_load(f)
            try:
                pop_subnet = data["subnet"]
            except:
                pass
            for device in data["devices"]:
                if ("efr" in device or "cfr" in device or "cfs" in device) and (
                    "lab" not in device and "den51" not in device
                ):
                    interfaces[device] = {}
                    interfaces[device]["interfaces"] = {}
                    link_type = data["devices"][device]
                    if "ul_longhaul" in data["devices"][device]:
                        for interface in data["devices"][device]["ul_longhaul"]:
                            interface["type"] = "longhaul"
                            interfaces[device]["interfaces"][
                                interface["iface"].lower()
                            ] = interface
                    if "ul_metro" in data["devices"][device]:
                        for interface in data["devices"][device]["ul_metro"]:
                            interface["type"] = "metro"
                            interfaces[device]["interfaces"][
                                interface["iface"].lower()
                            ] = interface
                    if "ul_local" in data["devices"][device]:
                        for interface in data["devices"][device]["ul_local"]:
                            interface["type"] = "local"
                            interfaces[device]["interfaces"][
                                interface["iface"].lower()
                            ] = interface
                    interfaces[device]["local_subnet"] = pop_subnet
    # pprint(interfaces)
    return interfaces


def get_interface_status(link_interfaces):
    pipe = my_redis.pipeline()
    for host, host_data in link_interfaces.items():
        for interface, interface_data in host_data["interfaces"].items():
            pipe.hgetall(
                "ports:{}:{}:interface_information".format(
                    host, interface_data["iface"].lower()
                )
            )
            try:
                for member in interface_data["members"]:
                    pipe.hgetall(
                        "ports:{}:{}:interface_information".format(
                            host, member["iface"].lower()
                        )    
                    )
            except:
                pass
    results = pipe.execute()

    interface_status = {}
    for result in results:
        if result:
            if result["device_name"] not in interface_status:
                interface_status[result["device_name"]] = {}
            interface_status[result["device_name"]][result["iface_name"].lower()] = {}

            interface_status[result["device_name"]][result["iface_name"].lower()][
                "oper_status"
            ] = result["oper_status"]

            print("{}:{}".format(result["device_name"], result["iface_name"]))

            try:
                td = datetime.timedelta(seconds=int(result["time_flapped"]))
                now = datetime.datetime.now()
                when_flapped = now - td
                interface_status[result["device_name"]][result["iface_name"].lower()][
                    "last_flapped"
                ] = when_flapped.strftime("%Y-%m-%d %X")
            except:
                interface_status[result["device_name"]][result["iface_name"].lower()][
                    "last_flapped"
                ] = "Unknown"
            print(
                interface_status[result["device_name"]][result["iface_name"].lower()][
                    "last_flapped"
                ]
            )

    for device, device_data in link_interfaces.items():
        for interface, interface_data in device_data["interfaces"].items():
            try:
                link_interfaces[device]["interfaces"][interface][
                    "oper_status"
                ] = interface_status[device][interface.lower()]["oper_status"]
                link_interfaces[device]["interfaces"][interface][
                    "last_flapped"
                ] = interface_status[device][interface.lower()]["last_flapped"]
            except:
                pass
            count = 0
            try:
                for member in interface_data["members"]:
                    try:
                        link_interfaces[device]["interfaces"][interface.lower()]["members"][
                            count
                        ]["oper_status"] = interface_status[device][
                            member["iface"].lower()
                        ][
                            "oper_status"
                        ]
                        link_interfaces[device]["interfaces"][interface.lower()]["members"][
                            count
                        ]["last_flapped"] = interface_status[device][
                            member["iface"].lower()
                        ][
                            "last_flapped"
                        ]
                    except:
                        pass
                    count = count + 1
            except:
                pass

    return link_interfaces


def build_links(link_interfaces):
    links = {}

    for device, device_data in link_interfaces.items():
        for interface, interface_data in link_interfaces[device]["interfaces"].items():
            if interface_data["type"] == "local":
                interface_data["ip"] = (
                    str(device_data["local_subnet"]) + str(interface_data["ip"]) + "/31"
                )

            if network(interface_data["ip"]) not in links:
                links[network(interface_data["ip"])] = {}
                links[network(interface_data["ip"])]["type"] = interface_data["type"]

            try:
                interface_data["member_count"] = len(interface_data["members"])
            except:
                interface_data["member_count"] = 0

            members = {}

            up_members = 0
            try:
                for member in interface_data["members"]:
                    if "vendor" in member:
                        vendor = member["vendor"]
                    else:
                        vendor = "N/A"
                    if "cid" in member:
                        cid = member["cid"]
                    else:
                        cid = "N/A"
                    if "oper_status" in member:
                        if member["oper_status"] == "up":
                            up_members = up_members + 1
        
                    members[member["iface"]] = {"vendor": vendor, "cid": cid}
            except:
                pass

            interface_data["up_members"] = up_members

            status = ""
            if up_members > 0:
                if up_members == interface_data["member_count"]:
                    status = "up"
                elif up_members < interface_data["member_count"]:
                    status = "degraded"
            else:
                status = "down"

            if interface_data["member_count"] == 1:
                if interface_data["type"] != "local":
                    interface_data["vendor"] = interface_data["members"][0]["vendor"]
                    interface_data["cid"] = interface_data["members"][0]["cid"]
                else:
                    interface_data["vendor"] = "N/A"
                    interface_data["cid"] = "N/A"

            else:
                vendors = []
                cids = []

                try:
                    for member in interface_data["members"]:
                        if interface_data["type"] != "local":
                            vendors.append(member["vendor"])
                            cids.append(member["cid"])
                        else:
                            vendors.append("N/A")
                            cids.append("N/A")
                except:
                    pass
                
                if all_same(vendors):
                    if interface_data["type"] != "local":
                        interface_data["vendor"] = interface_data["members"][0][
                            "vendor"
                        ]
                    else:
                        interface_data["vendor"] = "N/A"
                else:
                    interface_data["vendor"] = "Multiple"

                if all_same(cids):
                    if interface_data["type"] != "local":
                        interface_data["cid"] = interface_data["members"][0]["cid"]
                    else:
                        interface_data["cid"] = "N/A"
                else:
                    interface_data["cid"] = "Multiple"

            if "cid" not in interface_data:
                interface_data["cid"] = ""
            if "vendor" not in interface_data:
                interface_data["vendor"] = ""
            if "cost" not in interface_data:
                interface_data["cost"] = "N/A"
            if "oper_status" not in interface_data:
                interface_data["oper_status"] = "Unknown"
            if "last_flapped" not in interface_data:
                interface_data["last_flapped"] = "Unknown"

            if interface_data["ip"] == network(interface_data["ip"]):
                side = "a_side"
            else:
                side = "z_side"
            # pprint(interface_data)
            if "ams1" not in device and "fra1" not in device:
                links[network(interface_data["ip"])][side] = {
                    "address": interface_data["ip"],
                    "host": device,
                    "interface": interface_data["iface"],
                    "status": status,
                    "oper_status": interface_data["oper_status"],
                    "last_flapped": interface_data["last_flapped"],
                    "vendor": interface_data["vendor"],
                    "cid": interface_data["cid"],
                    "member_count": interface_data["member_count"],
                    "up_members": interface_data["up_members"],
                    "members": members,
                }

    # pprint(links)
    return links


@app.route("/")
def links_page():
    if request.args.get("status"):
        status = request.args.get("status")
    else:
        status = "degraded"
    if request.args.get("linktype"):
        linktype = request.args.get("linktype")
    else:
        linktype = ["metro", "longhaul", "local"]
    if request.args.get("refresh"):
        refresh = request.args.get("refresh")
    else:
        refresh = "60"
    link_interfaces = get_link_interfaces(config["pop_vars"])
    link_interfaces_with_status = get_interface_status(link_interfaces)
    links = build_links(link_interfaces_with_status)
    return render_template(
        "links.html", links=links, status=status, linktype=linktype, refresh=refresh
    )
